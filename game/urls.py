"""game URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from tic_tac_tue.views import *
from django.views.static import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),
    url(r'^save_peer_id/', save_peer_id),
    url(r'^go_online/', go_online),
    url(r'^go_offline/', go_offline),
    url(r'^set_language/', set_language, name='set_language'),
    url(r'^login/', login, name='login'),
    url(r'^registration/', registration, name='registration'),
    url(r'^go_to_free/', go_to_free),
    url(r'^check_player/', check_player),
    url(r'^game/(?P<id>\d+)/', game_room),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +\
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG is False:   #if DEBUG is True it will be served automatically
    urlpatterns += [
            url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
            url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}) 
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +\
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
