from django import forms
from .models import Player
from django.contrib.auth.models import User

class RegisterPlayer(forms.ModelForm):
	class Meta:
		model = Player
		fields = ['profile_img']


class RegisterUser(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ['username', 'password']