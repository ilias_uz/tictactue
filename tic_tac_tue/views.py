# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as fn_login
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.sessions.backends.db import SessionStore
# Import созданных ране классов моделя
from .models import Player, GameRoom
from .forms import RegisterPlayer, RegisterUser
from django.contrib.auth.models import User
from django.utils import translation
from django.utils.translation import ugettext as _

session = SessionStore()

# The login page
def login(request):
	username = request.POST.get('username')
	password = request.POST.get('password')
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			fn_login(request, user)
			return redirect('/')
		else:
			return HttpResponse("Invalid login details supplied")
	else:
		return render(request, 'login.html')

# The main page
def index(request):
	if not request.user.is_authenticated():
		return redirect('/login')
	else:
		user = Player.objects.get(user_info__username=request.user.username)
		if user.playing == True:
			Player.objects.filter(user_info__username=request.user.username).update(playing=False)
		if 'online_users' not in session:
			session['online_users'] = []
		online_users = session['online_users']
		players = Player.objects.filter(user_info__username__in=online_users)
		return render(request, 'index.html', {'players': players})

# Checking players are they free
@csrf_exempt 
def check_player(request):
	if request.method == 'POST':
		player_name = request.POST.get('player')
		user_name = request.POST.get('username')
		get_player = Player.objects.get(user_info__username=player_name)
		get_user = Player.objects.get(user_info__username=user_name)
		if get_player.playing == False:
			game_room = GameRoom.objects.create(author=user_name, player=player_name, author_peer_id=get_user.peer_id, player_peer_id=get_player.peer_id)
			Player.objects.filter(user_info__username=player_name).update(playing=True)
			Player.objects.filter(user_info__username=user_name).update(playing=True)
			return JsonResponse({'status': 'true', 'peer_id': get_player.peer_id, 'room_id': game_room.id, 'my_peer_id': get_user.peer_id})
		else:
			return HttpResponse('Игрок занет!')

# Saving peer id for to know other players.
# If other plyers whant play with you they will get your id from here for connect with you
@csrf_exempt 
def save_peer_id(request):
	peer_id = request.POST.get('peer_id')
	username = request.POST.get('username')
	Player.objects.filter(user_info__username=username).update(peer_id=peer_id)
	return HttpResponse('Сохранено!')

# Game room for two players
def game_room(request, id):
	game_room = GameRoom.objects.get(pk=id)
	return render(request, 'game.html', {'game_room': game_room})

# This function will work if player come to back home page
# For know user is online or not
@csrf_exempt
def go_online(request):
	username = request.POST.get('username')
	Player.objects.filter(user_info__username=username).update(playing=False)
	if 'online_users' not in session:
		session['online_users'] = [username]
	else:
		if username not in session['online_users']:
			session['online_users'].append(username)
	return HttpResponse('Сохранено!')

# This function will work if player close the page
# For know user is online or not
@csrf_exempt
def go_offline(request):
	username = request.user.username
	Player.objects.filter(user_info__username=username).update(playing=False)
	if username in session['online_users']:
		session['online_users'].remove(username)
		print session['online_users']
	return HttpResponse('Сохранено!')

# If user is not plaing but in db user playing is True 
# This function will work if Player out from game room 
@csrf_exempt
def go_to_free(request):
	username = request.POST.get('username')
	Player.objects.filter(user_info__username=username).update(playing=False)
	return HttpResponse('Сохранено!')

# Change language
@csrf_exempt
def set_language(request):
	language = request.POST.get('language')
	translation.activate(language)
	request.session[translation.LANGUAGE_SESSION_KEY] = language
	return redirect('/')

# User registration and add create PlayUser for registereted user
def registration(request):
	if request.method == 'POST':
		form_player = RegisterPlayer(request.POST, request.FILES)
		form_user = RegisterUser(request.POST)
		if 'profile_img' in request.FILES:
			if form_user.is_valid():
				user = form_user.save(commit=False)
				user.set_password(request.POST.get('password'))
				user.save()
				user_login = authenticate(username=request.POST['username'], password=request.POST['password'])
				fn_login(request, user_login)
			if form_player.is_valid():
				create_user = form_player.save(commit=False)
				create_user.user_info = User.objects.get(username=request.POST.get('username'))
				create_user.profile_img = request.FILES['profile_img']
				create_user.save()
				return redirect('/')
		else:
			avatar_upload_error = _('Аватар не найден!')
			form_user = RegisterUser()
			return render(request, 'registration.html', {'form_user': form_user, 'avatar_upload_error': avatar_upload_error})
	else:
		form_user = RegisterUser()
	return render(request, 'registration.html', {'form_user': form_user})