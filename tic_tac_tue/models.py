# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.

class Player(models.Model):
	user_info = models.OneToOneField(User)
	profile_img = models.FileField(upload_to='static/img_profile')
	playing = models.BooleanField(default=False)
	peer_id = models.CharField(max_length=100, default=True, editable=False)

	def __unicode__(self):
		return self.user_info.username


class GameRoom(models.Model):
	author = models.CharField(max_length=100)
	player = models.CharField(max_length=100)
	author_peer_id = models.CharField(max_length=100)
	player_peer_id = models.CharField(max_length=100)
	winer_user = models.CharField(max_length=100, default=True, blank=True)

	def __unicode__(self):
		return self.author

